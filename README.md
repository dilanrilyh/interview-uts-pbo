# 1. Mampu mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat. (Lampirkan link source code terkait

Penyelesaian masalah dalam pendekan matematika dan algoritma yang terkait ada beberapa yaitu :

1. Registrasi

Dimana user harus melalkukan login terlebih dahulu agar dapat masuk ke aplikasi 

```
import java.util.Scanner;

// Kelas abstrak yang mewakili kartu
abstract class Kartu {
    private String nomor;
    private String nama;
    private String email;
    private String pin;

    public Kartu(String nomor, String nama, String email, String pin) {
        this.nomor = nomor;
        this.nama = nama;
        this.email = email;
        this.pin = pin;
    }
 // Getter dan setter untuk detail kartu
    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNama() {
        return nama;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getPin() {
        return pin;
    }


// Metode abstrak untuk menampilkan informasi registrasi
    public abstract void tampilRegistrasi();

    // Metode abstrak untuk mengecek pulsa
    public abstract void cekPulsa();

    // Metode abstrak untuk mengisi pulsa
    public abstract void isiPulsa(int jumlah);

    // Metode abstrak untuk mengisi kuota
    public abstract void isiKuota(int pilihan);

    // Metode abstrak untuk mengecek kuota
    public abstract void cekKuota();
}

// Kelas Registrasi yang merupakan turunan dari kelas Kartu
class Registrasi extends Kartu {
    private int pulsa;
    private int kuotaHarian;
    private int kuotaMingguan;
    private int kuotaBulanan;

    public Registrasi(String nomor, String nama, String email, String pin) {
        super(nomor, nama, email, pin);
        this.pulsa = 0;
        this.kuotaHarian = 0;
        this.kuotaMingguan = 0;
        this.kuotaBulanan = 0;
    }
  println("Email       : " + getEmail());
        System.out.println("Pin         : " + getPin());
    }

 
```

2. Cek Pulsa

User dapat melakukan cek pulsa dengan cara memasuka no kartu dan jumlah pulsa dengan fungsi agar user dapat melihat info jumlah pulsanya

```
 @Override
    public void cekPulsa() {
        System.out.println("=== Cek Pulsa ===");
        System.out.println("Nomor kartu : " + getNomor());
        System.out.println("Pulsa       : " + pulsa);
    }
```

3. Isi Pulsa

User dapat melakukan isi pulsa agar dapat melakukan fungsi menu selanjutnya yaitu isi kuota

```
// Implementasi metode untuk mengisi pulsa
    @Override
    public void isiPulsa(int jumlah) {
        pulsa += jumlah;
        System.out.println("Pulsa berhasil diisi. Pulsa saat ini: " + pulsa);
    }
```

4. Isi Kuota

User dapat melakukan isi kuota dengan menampilkan beberapa menu baik kuota harian,mingguan maupun bulanan yang diambil dari jumlah kuota 

```
// Implementasi metode untuk mengisi kuota
    @Override
    public void isiKuota(int pilihan) {
        int hargaKuota = 0;
        int bonusKuotaAktif = 0;

        switch (pilihan) {
            case 1:
                hargaKuota = 10000;
                kuotaHarian += 500;
                break;
            case 2:
                hargaKuota = 25000;
                kuotaMingguan += 2048;
                bonusKuotaAktif = 512;  // Bonus kuota aktif untuk pembelian kuota mingguan
                break;
            case 3:
                hargaKuota = 75000;
                kuotaBulanan += 5120;
                bonusKuotaAktif = 1024; // Bonus kuota aktif untuk pembelian kuota bulanan
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                return;
        }

        if (pulsa >= hargaKuota) {
            pulsa -= hargaKuota;
            System.out.println("Kuota berhasil diisi. Kuota saat ini: " + getKuota(pilihan) + " MB");

            // Menampilkan bonus kuota aktif
            if (bonusKuotaAktif > 0) {
                System.out.println("Bonus kuota aktif: " + bonusKuotaAktif + " MB");
            }
        } else {
            System.out.println("Pulsa tidak mencukupi untuk mengisi kuota.");
        }
    }
 // Metode utilitas untuk mendapatkan kuota berdasarkan pilihan
    private int getKuota(int pilihan) {
        switch (pilihan) {
            case 1:
                return kuotaHarian;
            case 2:
                return kuotaMingguan;
            case 3:
                return kuotaBulanan;
            default:
                return 0;
        }
    }
```

5. Cek Kuota 

User dapat melakukan cek kuota agar user tau informasi kuota telah habis atau tidak maupun sudah tenggat atau belum

```
 // Implementasi metode untuk mengecek kuota
    @Override
    public void cekKuota() {
        System.out.println("=== Cek Kuota ===");
        System.out.println("Nomor kartu : " + getNomor());

        Scanner scanner = new Scanner(System.in);
        System.out.println("Pilih jenis kuota: ");
        System.out.println("1. Kuota Harian");
        System.out.println("2. Kuota Mingguan");
        System.out.println("3. Kuota Bulanan");
        System.out.print("Masukkan pilihan: ");
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca pilihan

        switch (pilihan) {
            case 1:
                System.out.println("Kuota Harian: " + kuotaHarian + " MB");
                break;
            case 2:
                System.out.println("Kuota Mingguan: " + kuotaMingguan + " MB");
                // Menampilkan bonus kuota aktif
                if (kuotaMingguan >= 2048) {
                    System.out.println("Bonus kuota aktif: 512 MB");
                }
                break;
            case 3:
                System.out.println("Kuota Bulanan: " + kuotaBulanan + " MB");
                // Menampilkan bonus kuota aktif
                if (kuotaBulanan >= 5120) {
                    System.out.println("Bonus kuota aktif: 1024 MB");
                }
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
    }
}
```

dari semua fungsi diatas dimana mendemonstrasikan penyelesaian masalah dengan pendekatan matematika dan algoritma pemrograman secara tepat telah dipenuhi dengan beberapa menu diatas seperti isi pulsa dan isi kuota.



# 2. Mampu menjelaskan algoritma dari solusi yang dibuat (Lampirkan link source code terkait)

Penggunaan algoritma pada kodingan di bawah memungkinkan untuk melakukan isi pulsa 

    
  `  public abstract void isiPulsa(int jumlah);`

```
  @Override
    public void isiPulsa(int jumlah) {
        pulsa += jumlah;
        System.out.println("Pulsa berhasil diisi. Pulsa saat ini: " + pulsa);
    }
```

Penggunaan algoritma pada kodingan di bawah memungkinkan untuk melakukan isi kuota 

`public abstract void isiKuota(int pilihan);`

```
// Implementasi metode untuk mengisi kuota
    @Override
    public void isiKuota(int pilihan) {
        int hargaKuota = 0;
        int bonusKuotaAktif = 0;

        switch (pilihan) {
            case 1:
                hargaKuota = 10000;
                kuotaHarian += 500;
                break;
            case 2:
                hargaKuota = 25000;
                kuotaMingguan += 2048;
                bonusKuotaAktif = 512;  // Bonus kuota aktif untuk pembelian kuota mingguan
                break;
            case 3:
                hargaKuota = 75000;
                kuotaBulanan += 5120;
                bonusKuotaAktif = 1024; // Bonus kuota aktif untuk pembelian kuota bulanan
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                return;
        }

        if (pulsa >= hargaKuota) {
            pulsa -= hargaKuota;
            System.out.println("Kuota berhasil diisi. Kuota saat ini: " + getKuota(pilihan) + " MB");

            // Menampilkan bonus kuota aktif
            if (bonusKuotaAktif > 0) {
                System.out.println("Bonus kuota aktif: " + bonusKuotaAktif + " MB");
            }
        } else {
            System.out.println("Pulsa tidak mencukupi untuk mengisi kuota.");
        }
    }
 // Metode utilitas untuk mendapatkan kuota berdasarkan pilihan
    private int getKuota(int pilihan) {
        switch (pilihan) {
            case 1:
                return kuotaHarian;
            case 2:
                return kuotaMingguan;
            case 3:
                return kuotaBulanan;
            default:
                return 0;
        }
    }

   // Implementasi metode untuk mengecek kuota
    @Override
    public void cekKuota() {
        System.out.println("=== Cek Kuota ===");
        System.out.println("Nomor kartu : " + getNomor());

        Scanner scanner = new Scanner(System.in);
        System.out.println("Pilih jenis kuota: ");
        System.out.println("1. Kuota Harian");
        System.out.println("2. Kuota Mingguan");
        System.out.println("3. Kuota Bulanan");
        System.out.print("Masukkan pilihan: ");
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca pilihan

        switch (pilihan) {
            case 1:
                System.out.println("Kuota Harian: " + kuotaHarian + " MB");
                break;
            case 2:
                System.out.println("Kuota Mingguan: " + kuotaMingguan + " MB");
                // Menampilkan bonus kuota aktif
                if (kuotaMingguan >= 2048) {
                    System.out.println("Bonus kuota aktif: 512 MB");
                }
                break;
            case 3:
                System.out.println("Kuota Bulanan: " + kuotaBulanan + " MB");
                // Menampilkan bonus kuota aktif
                if (kuotaBulanan >= 5120) {
                    System.out.println("Bonus kuota aktif: 1024 MB");
                }
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
    }
}
```
dari kode diatas kita dapat tahu bahwa algoritma dan penyelesaan masalah yang dibuat yaitu agar user dapat sesuai fungsinya dengan kebutuhan user

# 3. Mampu menjelaskan konsep dasar OOP
OOP sangat penting di perusahaan karena menurut saya dari cara kerjanya yang mudah dipahami dan terstruktur dalam membuat program. OOP membantu kita mengatur kode program dengan rapi, mirip seperti mengelompokkan benda-benda dalam kotak.

Misalnya ituu di perusahaan aplikasi perbankan, OOP memungkinkan kita membuat objek-objek seperti "Kartu" yang berisi informasi tentang nama dan no kartu. Kemudian kita juga bisa membuat objek "user" yang berisi data pribadi pelanggan dan daftar akun yang dimilikinya.

Dengan OOP, kita itu bisa mengelola objek-objek ini dengan mudah. Kita bisa mengubah atau menambah fitur pada objek tanpa mengacaukan bagian lain dari program. Jadi, jika semisalnya  perubahan pada akun atau pelanggan, kita tidak perlu mengedit seluruh program, hanya objek yang terkait saja.


# 4. Mampu mendemonstrasikan penggunaan Encapsulation secara tepat  (Lampirkan link source code terkait)

access modifier private, atribut pulsa, kuotaHarian,KuotaMingguan dan kuotaBulanan hanya bisa diakses oleh method di dalam kelas File.
dengan menggunakan method getter dan setter yang memiliki access modifier public, kita dapat mengakses dan mengubah nilai dari atribut tersebut dari luar kelas File

```
// Kelas Registrasi yang merupakan turunan dari kelas Kartu
class Registrasi extends Kartu {
    private int pulsa;
    private int kuotaHarian;
    private int kuotaMingguan;
    private int kuotaBulanan;

    public Registrasi(String nomor, String nama, String email, String pin) {
        super(nomor, nama, email, pin);
        this.pulsa = 0;
        this.kuotaHarian = 0;
        this.kuotaMingguan = 0;
        this.kuotaBulanan = 0;
    }
```

# 5. Mampu mendemonstrasikan penggunaan Abstraction secara tepat (Lampirkan link source code terkait)

```
// Metode abstrak untuk menampilkan informasi registrasi
    public abstract void tampilRegistrasi();
```
Pada kodingan di atas, penggunaan abstraction terlihat pada penggunaan kelas abstrak Storage yang memiliki method abstract tampilRegistrasi(); 

Method ini harus diimplementasikan di kelas turunan yaitu kelas Register

   ```
 // Metode abstrak untuk mengecek pulsa
    public abstract void cekPulsa();
```
Pada kodingan di atas, penggunaan abstraction terlihat pada penggunaan kelas abstrak Storage yang memiliki method abstract cekPulsa(); 

Method ini harus diimplementasikan di kelas turunan yaitu kelas cekPulsa

    
```
// Metode abstrak untuk mengisi pulsa
    public abstract void isiPulsa(int jumlah);
```
Pada kodingan di atas, penggunaan abstraction terlihat pada penggunaan kelas abstrak Storage yang memiliki method abstract isiPulsa(); 

Method ini harus diimplementasikan di kelas turunan yaitu kelas isiPulsa



```
// Metode abstrak untuk mengisi kuota
    public abstract void isiKuota(int pilihan);
```



Pada kodingan di atas, penggunaan abstraction terlihat pada penggunaan kelas abstrak Storage yang memiliki method abstract isiKuota(); 

Method ini harus diimplementasikan di kelas turunan yaitu kelas isiKuota

   
```
// Metode abstrak untuk mengecek kuota
    public abstract void cekKuota();
```
Pada kodingan di atas, penggunaan abstraction terlihat pada penggunaan kelas abstrak Storage yang memiliki method abstract cekKuota(); 

Method ini harus diimplementasikan di kelas turunan yaitu kelas cekKuota

Dengan menggunakan kelas abstrak dan method abstract, kita dapat mengabstraksi fungsionalitas dan memastikan bahwa kelas turunan mengimplementasikan method-method yang diperlukan dengan cara yang sesuai.



# 6. Mampu mendemonstrasikan penggunaan Inheritance dan Polymorphism secara tepat  (Lampirkan link source code terkait)

- Inheritance

```
// Kelas Registrasi yang merupakan turunan dari kelas Kartu
class Registrasi extends Kartu {
    private int pulsa;
    private int kuotaHarian;
    private int kuotaMingguan;
    private int kuotaBulanan;
```
Pada kodingan di atas, penggunaan Inheritance terlihat pada deklarasi kelas turunan yaitu kelas Registrasi dengan menggunakan keyword "extends", untuk mewarisi sifat-sifat dari kelas induk yaitu kelas abstrak Kartu. Dengan demikian, kelas turunan memiliki akses ke atribut dan method yang ada di kelas induknya.

- Polymorphism

```
// Implementasi metode untuk menampilkan informasi registrasi
    @Override
    public void tampilRegistrasi() {
        System.out.println("=== Silahkan lakukan registrasi kartu ===");
        System.out.println("Nomor kartu : " + getNomor());
        System.out.println("Nama        : " + getNama());
        System.out.println("Email       : " + getEmail());
        System.out.println("Pin         : " + getPin());
    }
```
Penggunaan Polymorphism terlihat pada penggunaan  penggunaan method override pada kelas tampilRegistrasi. Method tampilRegistrasi() pada kelas registrasi merupakan method abstract yang harus diimplementasikan di kelas turunan.

 ```
// Implementasi metode untuk mengecek pulsa
    @Override
    public void cekPulsa() {
        System.out.println("=== Cek Pulsa ===");
        System.out.println("Nomor kartu : " + getNomor());
        System.out.println("Pulsa       : " + pulsa);
    }
```
Penggunaan Polymorphism terlihat pada penggunaan  penggunaan method override pada kelas cekPulsa. Method cekPulsa() pada kelas cekPulsa merupakan method abstract yang harus diimplementasikan di kelas turunan, yaitu kelas cekPulsa.

 ```
// Implementasi metode untuk mengisi pulsa
    @Override
    public void isiPulsa(int jumlah) {
        pulsa += jumlah;
        System.out.println("Pulsa berhasil diisi. Pulsa saat ini: " + pulsa);
    }

 // Implementasi metode untuk mengisi kuota
    @Override
    public void isiKuota(int pilihan) {
        int hargaKuota = 0;
        int bonusKuotaAktif = 0;

        switch (pilihan) {
            case 1:
                hargaKuota = 10000;
                kuotaHarian += 500;
                break;
            case 2:
                hargaKuota = 25000;
                kuotaMingguan += 2048;
                bonusKuotaAktif = 512;  // Bonus kuota aktif untuk pembelian kuota mingguan
                break;
            case 3:
                hargaKuota = 75000;
                kuotaBulanan += 5120;
                bonusKuotaAktif = 1024; // Bonus kuota aktif untuk pembelian kuota bulanan
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                return;
        }

        if (pulsa >= hargaKuota) {
            pulsa -= hargaKuota;
            System.out.println("Kuota berhasil diisi. Kuota saat ini: " + getKuota(pilihan) + " MB");

            // Menampilkan bonus kuota aktif
            if (bonusKuotaAktif > 0) {
                System.out.println("Bonus kuota aktif: " + bonusKuotaAktif + " MB");
            }
        } else {
            System.out.println("Pulsa tidak mencukupi untuk mengisi kuota.");
        }
    }
```

Penggunaan Polymorphism terlihat pada penggunaan  penggunaan method override pada kelas isiPulsa. Method isiPulsa() pada kelas isiPulsa merupakan method abstract yang harus diimplementasikan di kelas turunan, yaitu kelas isiPulsa.


```
   // Implementasi metode untuk mengecek kuota
    @Override
    public void cekKuota() {
        System.out.println("=== Cek Kuota ===");
        System.out.println("Nomor kartu : " + getNomor());

        Scanner scanner = new Scanner(System.in);
        System.out.println("Pilih jenis kuota: ");
        System.out.println("1. Kuota Harian");
        System.out.println("2. Kuota Mingguan");
        System.out.println("3. Kuota Bulanan");
        System.out.print("Masukkan pilihan: ");
        int pilihan = scanner.nextInt();
        scanner.nextLine(); // Membersihkan newline character setelah membaca pilihan

        switch (pilihan) {
            case 1:
                System.out.println("Kuota Harian: " + kuotaHarian + " MB");
                break;
            case 2:
                System.out.println("Kuota Mingguan: " + kuotaMingguan + " MB");
                // Menampilkan bonus kuota aktif
                if (kuotaMingguan >= 2048) {
                    System.out.println("Bonus kuota aktif: 512 MB");
                }
                break;
            case 3:
                System.out.println("Kuota Bulanan: " + kuotaBulanan + " MB");
                // Menampilkan bonus kuota aktif
                if (kuotaBulanan >= 5120) {
                    System.out.println("Bonus kuota aktif: 1024 MB");
                }
                break;
            default:
                System.out.println("Pilihan tidak valid.");
                break;
        }
    }
}
```
Penggunaan Polymorphism terlihat pada penggunaan  penggunaan method override pada kelas isiKuota dan Folder. Method isiKuota() pada kelas isiKuota merupakan method abstract yang harus diimplementasikan di kelas turunan, yaitu kelas isiKuota.

Dengan menggunakan konsep polymorphism, kita dapat memperlakukan objek dari kelas turunan sebagai objek dari kelas induk, sehingga memudahkan dalam memanipulasi objek-objek tersebut secara umum.


# 7. Mampu menerjemahkan proses bisnis ke dalam skema OOP secara tepat. Bagaimana cara Kamu mendeskripsikan proses bisnis (kumpulan use case) ke dalam OOP ?

Langkah-langkah untuk mendeskripsikan proses bisnis ke dalam OOP yaitu:

1. Identifikasi objek-objek yang terlibat:

Dalam setiap use case, terdapat objek-objek yang terlibat dalam proses bisnis. Identifikasi objek-objek tersebut berdasarkan fitur-fitur yang ada dalam setiap use case.

2. Tentukan kelas-kelas:

Setelah mengidentifikasi objek-objek, tentukan kelas-kelas yang mewakili objek-objek tersebut. Setiap kelas akan memiliki atribut dan metode yang sesuai dengan fungsionalitasnya.

3. Definisikan atribut dan metode:

Untuk setiap kelas, definisikan atribut-atribut yang diperlukan untuk menyimpan informasi yang relevan. Misalnya, untuk kelas Folder, atribut dapat berupa nama folder, daftar file, dan kapasitas penyimpanan.
Selanjutnya, definisikan metode-metode yang akan mengimplementasikan fungsionalitas yang terkait dengan setiap use case. Misalnya, untuk kelas Folder, metode-metode dapat berupa UploadFile(), hapusFile(), tampilkanDaftarFile(), dan sebagainya.

4. Hubungkan antara kelas:

Identifikasi hubungan dan keterkaitan antara kelas-kelas yang ada. Misalnya, kelas File dapat terhubung dengan kelas Folder melalui asosiasi atau komposisi, di mana Folder memiliki objek-objek File yang ada di dalamnya.

5. Implementasikan logika:

Implementasikan logika yang terkait dengan setiap use case dalam metode-metode yang telah didefinisikan dalam kelas-kelas. Logika bisnis ini harus memastikan bahwa fungsionalitas yang diinginkan dalam setiap use case tercapai.

6. Tes dan validasi:

Setelah implementasi selesai, lakukan pengujian dan validasi untuk memastikan bahwa fungsionalitas yang diharapkan berjalan dengan baik dan sesuai dengan kebutuhan bisnis.

# 8. Mampu menjelaskan rancangan dalam bentuk Class Diagram, dan Use Case table  (Lampirkan diagram terkait)
![Dokumentasi](https://gitlab.com/dilanrilyh/interview-uts-pbo/-/raw/main/dokumentasi/Screenshot_2023-07-15_230206.png)
# 9. Mampu memberikan gambaran umum aplikasi kepada publik menggunakan presentasi berbasis video   (Lampirkan link Youtube terkait)

https://youtu.be/xNWsFaB9IZ8

# 10. Inovasi UX (Lampirkan url screenshot aplikasi di Gitlab / Github)

1. Tampilan Register
![Dokumentasi](Dokumentasi/regis.png)

2. Tampilan Cek Pulsa
![Dokumentasi](Dokumentasi/cpulsa.png)

3. Tampilan Isi Pulsa
![Dokumentasi](Dokumentasi/ipulsa.png)

4. Tampilan Isi Kuota
![Dokumentasi](Dokumentasi/ikuota.png)

5. Tampilan cek Kuota

![Dokumentasi](Dokumentasi/ckuota.png)
